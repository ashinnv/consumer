module gitlab.com/ashinnv/consumer

go 1.17

require (
	gitlab.com/ashinnv/okoframe v0.0.0-20210916124110-2939cd084d7b
	gitlab.com/ashinnv/okonet v0.0.0-20210916124258-598c1765e101
	gocv.io/x/gocv v0.28.0
)

replace "gitlab.com/ashinnv/okoconf v0.0.0-20210917180342-39f248c44522" => ../okoconf

require gitlab.com/ashinnv/okolog v0.0.0-20210916124216-958d7d3b1cee // indirect
