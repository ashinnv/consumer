package main

import (
	"fmt"
	"strconv"

	"gocv.io/x/gocv"

	//"gitlab.com/ashinnv/okoconf"
	//"../okoconf"
	"gitlab.com/ashinnv/okoframe"
	"gitlab.com/ashinnv/okonet"
)

type matCont struct {
	Mat gocv.Mat
	Count int
}

//var conf okoconf.ConfBucket
//var conConf okoconf.ConsumerConf

func main() {
	var cams []int = []int{1, 2, 3, 4}
	var sendTarget string = "localhost:8081"
	var transformerCount = 2
	//conConf.ExeIndex = 1

	framePipe := make(chan okoframe.Frame)
	matPipe := make(chan matCont)

	go okonet.FrameSender(sendTarget, framePipe)
	for _, cam := range cams {
		go runCam(matPipe, framePipe, cam, transformerCount)
	}

}

func transformer(matPipe chan matCont, framePipe chan okoframe.Frame, camIndx int) {
	for mt := range matPipe {
		go okoframe.CreateFrameFromMat(mt.Mat, strconv.Itoa(camIndx), "1", "ashinnv", "a3hfsnvo8he",strconv.Itoa(mt.Count))//conConf.UnitId, conf.UserName, conf.PassHash,strconv.Itoa(mt.Count))
	}
}

func runCam(matPipe chan matCont, framePipe chan okoframe.Frame, camIndx int, transformerCount int) {
	var tmpMat = gocv.NewMat()

	cam, err := gocv.VideoCaptureDevice(camIndx)
	if err != nil{
		fmt.Printf("Can't get camera index %d. Retry routine is not implemented.", camIndx)
	}

	cam.Read(&tmpMat);

}